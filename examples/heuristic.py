"""Train a controller for waypoint following.

This example depends on gradient_free_rl: https://gitlab.com/da_doomer/gradient-free-rl

"""
from pathlib import Path
import json
from marble_control.board import Board
from marble_control.logging import images_to_video
from marble_control.logging import save_pixel_array
from marble_control_gym import RandomBoardEnv


def env_maker():
    return RandomBoardEnv()


def heuristic(state):
    (vx, vy, dx, dy, vxt, vyt) = state[:6]
    return (dx, dy)


def rollout(
        policy,
        env_maker,
        steps: int,
        json_path: Path = None,
        video_path: Path = None,
        ):
    fps = 60
    if video_path is not None:
        if video_path.exists():
            video_path.unlink()
    filepaths = list()

    # Simulate board
    env = env_maker()
    observation = env.reset()
    data = list()
    for i in range(steps):
        print(f"{i}/{steps}")

        # Act
        action = policy(observation)
        observation_next, _, done, _ = env.step(action)

        # Save file for animation
        if video_path is not None:
            filename = (video_path.parent/str(i).rjust(10)).with_suffix(".png")
            save_pixel_array(env.render("rgb_array"), filename)
            filepaths.append(filename)

        # Save transition tuple
        data.append(dict(
            observation=observation,
            action=action,
            observation_next=observation_next,
        ))
        observation = observation_next

        if done:
            break

    # Save video and clean up
    if video_path is not None:
        images_to_video(video_path.parent, video_path, fps)
        for filename in filepaths:
            filename.unlink()

    # Save state evolution
    if json_path is not None:
        with open(json_path, "wt") as f_p:
            f_p.write(json.dumps(data, indent=4))
        print(f"Saved {json_path}")


def main():
    """Train a marble in random boards and record a video."""
    steps = 600
    json_path = Path("transitions.json")
    video_path = None
    rollout(
        heuristic,
        env_maker,
        steps,
        json_path=json_path,
        video_path=video_path
    )


if __name__ == "__main__":
    main()
