"""Random agent on random marble board."""
from pathlib import Path
from marble_control.board import Board
from marble_control.logging import images_to_video
from marble_control.logging import save_pixel_array
from marble_control_gym import RandomBoardEnv


def main():
    """Throw a marble in a random board and record a video."""
    fps = 60
    steps = 60
    video_path = Path("video.mp4")
    if video_path.exists():
        video_path.unlink()
    filepaths = list()

    # Build board
    env = RandomBoardEnv()

    # Simulate board
    for i in range(steps):
        print(f"{i}/{steps}")
        action = env.action_space.sample()
        observation, _, done, _ = env.step(action)
        filename = (video_path.parent/str(i).rjust(10)).with_suffix(".png")
        save_pixel_array(env.render("rgb_array"), filename)
        filepaths.append(filename)
        if done:
            break

    # Save video and clean up
    images_to_video(video_path.parent, video_path, fps)
    for filename in filepaths:
        filename.unlink()


if __name__ == "__main__":
    main()
