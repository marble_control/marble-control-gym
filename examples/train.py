"""Train a controller for waypoint following.

This example depends on gradient_free_rl: https://gitlab.com/da_doomer/gradient-free-rl

"""
from pathlib import Path
import math
from marble_control.board import Board
from marble_control.logging import images_to_video
from marble_control.logging import save_pixel_array
from marble_control_gym import RandomBoardEnv
from gradient_free_rl.optimizers import GeneticOptimizer
from gradient_free_rl.policies import MLPPolicy
from gradient_free_rl.spaces import Space


def env_maker():
    return RandomBoardEnv()


def heuristic(state):
    (vx, vy, dx, dy, vxt, vyt) = state
    return (dx, dy)


def train_mlp(env_maker) -> MLPPolicy:
    """Train an MLPPolicy on the given environment."""
    # Create a multi-layer perceptron and a genetic optimizer
    observation_space = Space.from_gym(env_maker().observation_space)
    action_space = Space.from_gym(env_maker().action_space)
    policy = MLPPolicy.from_uniform(
        inner_sizes=[4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4],
        activation="tanh",
        mutation_stdev=0.1,
        observation_space=observation_space,
        action_space=action_space,
    )
    optimizer = GeneticOptimizer(
        mutated_n=1000,
        crossover_n=1000,
        make_env=env_maker,
        rollout_len=120,
        rollout_n=1,
        elite_n=5,
    )

    # Iterate the optimization algorithm
    policy = optimizer.optimized(policy, timesteps=1e3)
    return policy


def plot(policy, env_maker):
    fps = 60
    steps = 600
    video_path = Path("video.mp4")
    if video_path.exists():
        video_path.unlink()
    filepaths = list()

    # Simulate board
    env = env_maker()
    observation = env.reset()
    for i in range(steps):
        print(f"{i}/{steps}")
        action = policy(observation)
        observation, _, done, _ = env.step(action)
        filename = (video_path.parent/str(i).rjust(10)).with_suffix(".png")
        save_pixel_array(env.render("rgb_array"), filename)
        filepaths.append(filename)
        if done:
            observation = env.reset()

    # Save video and clean up
    images_to_video(video_path.parent, video_path, fps)
    for filename in filepaths:
        filename.unlink()


def main():
    """Train a marble in random boards and record a video."""
    #mlp = train_mlp(env_maker)
    plot(heuristic, env_maker)


if __name__ == "__main__":
    main()
