"""Wrap a marble_control board as an OpenAI Gym environment."""
from dataclasses import dataclass
from dataclasses import field
import random
import math
import copy
import gym
import pymunk
from marble_control.board import Board


@dataclass
class RandomBoardEnv(gym.Env):
    """Wrap a marble_control board as an OpenAI Gym environment. Observations
    are the state of the board and actions are forces applied on the
    marble."""
    marble_velocity_range: float = 0.0
    marble_mass: float = 1.0
    marble_radius: float = 0.5
    marble_elasticity: float = 1.0
    marble_max_force: float = 500.0
    marble_max_speed: float = 10.0
    polygon_vertex_n: int = 5
    polygon_position_scale: float = 50.0
    polygon_size_scale: float = 20.0
    polygon_elasticity: float = 0.8
    polygon_friction: float = 0.9
    board_polygon_n: int = 0
    board_size: tuple[float, float] = (50.0, 50.0)
    board_dt: float = 1.0/60.0
    board_frame_skip: int = 1
    board_walls: bool = True
    board_wall_thickness: float = 1.0
    board_waypoint_n: int = 3
    board_waypoint_radius: float = 1.0
    board_waypoint_velocity_scale: float = 2.0
    reward_waypoint_reach: float = 100.0
    lidar_n: int = 8
    board: Board = field(init=False)

    def __post_init__(self):
        self.reset()

    @property
    def observation_space(self) -> gym.spaces.Box:
        """Observations are continuous representations of the system."""
        return gym.spaces.Box(
                low=-math.inf, high=math.inf, shape=(len(self.observation),)
            )

    @property
    def action_space(self) -> gym.spaces.Box:
        """Actions are continuous forces applied to the marble."""
        return gym.spaces.Box(low=-1.0, high=1.0, shape=(2,))

    @property
    def lidar(self) -> tuple[float, ...]:
        # Split an arc, then for each angle run a ray-trace starting in the
        # marble and testing for hits with every shape in the board
        angles = [i*math.pi/self.lidar_n for i in range(self.lidar_n)]
        lidar = list()
        ray_length = max(self.board.size)
        ray_start = self.board.marble.position
        for angle in angles:
            new_x = ray_length*math.cos(angle)
            new_y = ray_length*math.sin(angle)
            ray_end = pymunk.Vec2d(*ray_start) + pymunk.Vec2d(new_x, new_y)
            collision_distances = [1.0]
            for shape in self.board.pymunk_obstacle_shapes:
                segment_query_info = shape.segment_query(
                    ray_start,
                    ray_end,
                )
                is_ray_collision = segment_query_info.shape is not None
                if is_ray_collision:
                    collision_distance = segment_query_info.alpha
                    collision_distances.append(collision_distance)
            lidar_entry = min(collision_distances)
            lidar.append(lidar_entry)
        return tuple(lidar)

    @property
    def observation(self) -> tuple[float, ...]:
        """The observation consists of
            (vx, vy, dx, dy, vxt, vyt)
        where (vx, vy) is the marble's velocity vector, (dx, dy) is the
        relative position of the target and (vxt, vyt) is the target
        velocity."""
        (vx, vy) = self.board.marble.velocity
        if len(self.board.waypoints) > 0:
            (x0, y0) = self.board.marble.position
            (x, y) = self.board.waypoints[0].position
            (dx, dy) = (x-x0, y-y0)
            (vxt, vyt) = self.board.waypoints[0].velocity
        else:
            (dx, dy) = (0, 0)
            (vxt, vyt) = (0, 0)
        dis = max(self.board.size)
        return (
            vx/dis,
            vy/dis,
            dx/dis,
            dy/dis,
            vxt/dis,
            vyt/dis,
            *self.lidar,
        )

    @property
    def done(self) -> bool:
        """The environment terminates when there are no more waypoints in the
        board."""
        return len(self.board.waypoints) == 0

    def reward(self, board1, board2, action: tuple[float, float]) -> float:
        """Reward is given for moving towards the waypoints at the correct
        velocity."""
        # Reward moving towards the waypoints
        if len(board1.waypoints) > 0:
            target_position = board1.waypoints[0].position
        else:
            target_position = board1.marble.position
        distance_1 = math.dist(
                board1.marble.position,
                target_position,
            )
        distance_2 = math.dist(
                board2.marble.position,
                target_position,
            )
        distance_reward = (distance_1-distance_2)/max(board1.size)
        # TODO reward reaching waypoints with the correct velocity
        velocity_reward = 0.0

        # Reward reaching waypoints
        if len(board1.waypoints) > len(board2.waypoints):
            waypoint_reach_reward = self.reward_waypoint_reach
        else:
            waypoint_reach_reward = 0.0
        return sum((
                distance_reward,
                velocity_reward,
                waypoint_reach_reward
            ))

    @property
    def info(self) -> dict:
        """The info dict is always empty."""
        return dict()

    def reset(self):
        # Resetting is just building a new board and a marble with random
        # initial velocity
        vx = random.uniform(
                -self.marble_velocity_range,
                self.marble_velocity_range
            )
        vy = random.uniform(
                -self.marble_velocity_range,
                self.marble_velocity_range
            )
        marble_velocity = (vx, vy)
        self.board = Board.random(
                marble_velocity=marble_velocity,
                marble_mass=self.marble_mass,
                marble_radius=self.marble_radius,
                marble_elasticity=self.marble_elasticity,
                polygon_n=self.board_polygon_n,
                vertex_n=self.polygon_vertex_n,
                polygon_position_scale=self.polygon_position_scale,
                polygon_size_scale=self.polygon_size_scale,
                polygon_elasticity=self.polygon_elasticity,
                polygon_friction=self.polygon_friction,
                size=self.board_size,
                dt=self.board_dt,
                frame_skip=self.board_frame_skip,
                walls=self.board_walls,
                wall_thickness=self.board_wall_thickness,
                waypoint_n=self.board_waypoint_n,
                waypoint_radius=self.board_waypoint_radius,
                waypoint_velocity_scale=self.board_waypoint_velocity_scale,
            )
        return self.observation

    def step(self, action: tuple[float, float]):
        # Save current state
        board1 = copy.copy(self.board)

        # Stepping is applying the given force to the marble, scaled by the
        # maximum force.
        (fxr, fyr) = (
                min(max(action[0], -1), 1),
                min(max(action[1], -1), 1)
            )
        (fx, fy) = (fxr*self.marble_max_force, fyr*self.marble_max_force)
        self.board.apply_force_marble((fx, fy))
        self.board.step()

        # If the marble speed is greater than the maximum velocity, clip it
        speed = math.dist((0, 0), self.board.marble.velocity)
        if speed > self.marble_max_speed:
            (vx, vy) = self.board.marble.velocity
            new_velocity = (
                    vx/speed*self.marble_max_speed,
                    vy/speed*self.marble_max_speed
                )
            self.board.marble.velocity = new_velocity

        # Compute reward using new state and old state
        board2 = copy.copy(self.board)
        reward = self.reward(board1, board2, action)
        return (
                self.observation,
                reward,
                self.done,
                self.info
            )

    def render(self, mode="rgb_array"):
        if mode != "rgb_array":
            raise ValueError(
                f"Only 'rgb_array' render mode supported, received '{mode}'."
                )
        return self.board.pixel_array
